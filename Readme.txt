                          CS430 PROJECT README

Zip file includes all the documents and code files of the application. This application can be used to compute the cover S' with minimum cost as requested by the problem of the project.This file contains a summary of what you will find in each of the files that make up our program.

-----------------------------------------
1. Development Environment
-----------------------------------------

This application is developed in Microsoft Visual Studio 2012 with .NET Framework 4.5, using C#.

This application is compiled under Windows 7 SP1 x64, the application can run on both x86 and x64 operation system. 

------------------------------------------
2. System Requirement
------------------------------------------

Processor:                                Intel(R) Pentium or higher processor

Installed Memory:                                            2.00 GB or higher

Operation System:                      Microsoft Vista Home Premimum or higher

Display:                                                  1024 x 768 or higher

------------------------------------------
3. Installation
------------------------------------------

(1) Install Microsoft .NET Framework 4.5

If you have installed this framework, or you installed Microsoft Visual Studio 2012, you can skip this step.

Download Microsoft .NET Framework 4.5 from http://www.microsoft.com/en-us/download/details.aspx?id=30653

Install it.

(2) Get latest release of this app

Get the app from https://bitbucket.org/gatesice/iit-cs430-project/downloads/release-win32.zip

Extract the zip package to where you want to install.

------------------------------------------
4. Execution
------------------------------------------

(a) gui.exe

This program is designed for running the algorithm. After run the program. In the program window, pick a file and open it. Then click on 'Go!!!' button to start running the algorithm.

The result will show in richtext box at bottom right of the window.

The richtext box in bottom left area shows the content of input file.

(b) algo.dll

This dll file contains the data structure and algorithm for the project. Developer can develop ui to show the algorithm running result by adding event handler to Algorithm.PostOutput.

(c) input.exe

This is an addition program that can generate input files. User can click on the board to set points as 'Targets' and 'Sensors' that we want.

After set all points, user should give a range of sensor cost that the program will give those sensors randomly while saving the input file.

After all things are done, click on save and the program will first delete all Targets which not containd by any Sensors(There will be a notice when there exists such targets). Then you can save your input file.

input.exe don't follow the algorithm and just simple detect the containing conditions. So if there are too much points on it, it'll cost a little longer to process the saving process.

------------------------------------------
5. Copyright
------------------------------------------

This project is worked out by Lu Wang, Yixuan Zhao and Jiaqi Chen. The source code and release can be found at https://bitbucket.org/gatesice/iit-cs430-project .