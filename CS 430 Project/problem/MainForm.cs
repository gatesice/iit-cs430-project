﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace problem
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            filePicker.FileOk += filePicker_FileOk;
        }

        #region Pick File and get file content.
        /// <summary>
        /// When user picked a file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void filePicker_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                txtFilename.Text = filePicker.FileName == null ? "No file exists!" : filePicker.FileName;
                using (System.IO.StreamReader reader = new System.IO.StreamReader(filePicker.FileName))
                {
                    inputFileContent = reader.ReadToEnd();
                    rtInput.Text = InputFileContent;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error when proceeding open file:\n" + ex.ToString());
            }
        }

        /// <summary>
        /// When user press 'Pick File'. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            filePicker.ShowDialog();
        }


        private string inputFileContent;
        /// <summary>
        /// Get the content of input file.
        /// </summary>
        public string InputFileContent { get { if (inputFileContent == null) { return null; } return inputFileContent; } }
        #endregion

        #region Run algorithm.
        /// <summary>
        /// When user press 'Go!!!'.
        /// This starts the algorithm.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGo_Click(object sender, EventArgs e)
        {
            if (btnGoStatus)    // needs to run.
            {
                rtOutput.Clear();
                algorithmThread = new System.Threading.Thread(runAlgorithm);
                algorithmThread.Start();
                BtnGoStatus = false;
            }
            else                // needs to stop
            {
                try
                {
                    algorithmThread.Abort();
                }
                catch
                {
                    System.Diagnostics.Debug.WriteLine("algorithmThread Failed to Abort. Is it already stopped?");
                }
                BtnGoStatus = true;
            }
        }
        private bool btnGoStatus = true;
        private bool BtnGoStatus
        {
            get
            {
                return btnGoStatus;
            }

            set
            {
                btnGoStatus = value;
                if (btnGoStatus) { 
                    btnGo.Text = "Go!!!"; 
                }
                else { btnGo.Text = "Stop!!!"; }
                btnPickFile.Enabled = btnGoStatus;
            }
        }
        delegate void setBtnGoStatusDelegate(bool status);
        private void setBtnGoStatus(bool status)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new setBtnGoStatusDelegate(setBtnGoStatus), status);
            }
            else
            {
                BtnGoStatus = status;
            }
        }

        private System.Threading.Thread algorithmThread;

        /// <summary>
        /// Get the algorithm to solve the problem.
        /// </summary>
        private void runAlgorithm()
        {
            if (InputFileContent == null) { BtnGoStatus = true; return; }
            List<Target> tList = new List<Target>();
            List<Sensor> sList = new List<Sensor>();

            lock (inputFileContent)
            {
                System.Text.RegularExpressions.Regex regexInputfileLines = 
                    new System.Text.RegularExpressions.Regex(@"([TS]) (-?\d+\.?\d*) (-?\d+\.?\d*) (\d+)");

                System.Text.RegularExpressions.MatchCollection mc =
                    regexInputfileLines.Matches(InputFileContent);

                foreach (System.Text.RegularExpressions.Match m in mc)
                {
                    try
                    {
                        if (m.Groups[1].Value == "T")
                        {
                            tList.Add(new Target(
                                "T" + (tList.Count + 1).ToString(),
                                float.Parse(m.Groups[2].Value),
                                float.Parse(m.Groups[3].Value)));
                        }
                        else if (m.Groups[1].Value == "S")
                        {
                            sList.Add(new Sensor(
                                "S" + (sList.Count + 1).ToString(),
                                float.Parse(m.Groups[2].Value),
                                float.Parse(m.Groups[3].Value),
                                int.Parse(m.Groups[4].Value)));
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine("An error line in inputfile!\n" + ex.Message);
                    }
                }
            }

            Algorithm algorithm = new Algorithm(tList.ToArray(), sList.ToArray());
            algorithm.PostOutput += algorithm_PostOutput;

            algorithm.Do();
            setBtnGoStatus(true);
        }

        void rtOutputAddText(string s) { 
            rtOutput.Text += s; 
            rtOutput.Text += Environment.NewLine;
            rtOutput.SelectionStart = rtOutput.Text.Length;
            rtOutput.ScrollToCaret();
        }
        delegate void rtOutputAddTextDelegate(string s);
        void algorithm_PostOutput(object sender, PostOutputEventArgs e)
        {
            foreach (var s in e.Lines)
            {
                if (this.InvokeRequired)
                {
                    this.Invoke(new rtOutputAddTextDelegate(rtOutputAddText), s);
                }
                else
                {
                    rtOutputAddText(s);
                }
            }
        }

        #endregion
    }
}
