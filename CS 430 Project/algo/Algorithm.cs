﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace problem
{
    public abstract class Dot
    {
        public Dot(string name, float x, float y)
        {
            this.name = name;
            this.x = x;
            this.y = y;
        }

        private string name;
        public string Name { get { return name; } }

        private float x, y;
        public float X { get { return x; } }
        public float Y { get { return y; } }

        public static float operator - (Dot a, Dot b)
        {
            return Convert.ToSingle(Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2)));
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }

    public class Target : Dot
    {
        public Target(string name, float x, float y) : base(name, x, y) { }

        public override string ToString()
        {
            return string.Format("Target {2} locates at ({0},{1})", X, Y, Name);
        }
    }

    public class Sensor : Dot
    {
        /// <summary>
        /// A sensor.
        /// </summary>
        /// <param name="x">X coordinate of the sensor.</param>
        /// <param name="y">Y coordinate of the sensor.</param>
        /// <param name="cost">Cost of the sensor.</param>
        public Sensor(string name, float x, float y, int cost)
            : base(name, x, y)
        {
            this.cost = cost;
        }
        
        private int cost;
        public int Cost { get { return cost; } }

        public override string ToString()
        {
            return string.Format("Sensor {3} locates at ({0},{1}), it has a cost of {2}", X, Y, Cost, Name);
        }
    }

    public class SensorSet
    {
        /// <summary>
        /// Create a set of sensor with no sensors, and it will have a min value of integer, represents 'infinity'.
        /// </summary>
        public SensorSet()
        {
            sensors = new List<Sensor>();
            cost = SensorSet.InfiniteCost;
        }

        /// <summary>
        /// Create a set of sensor with one sensor.
        /// </summary>
        /// <param name="newSensor">The sensor in it.</param>
        public SensorSet(Sensor newSensor)
        {
            sensors = new List<Sensor>(1);
            sensors.Add(newSensor);

            cost = sensors[0].Cost;
        }

        /// <summary>
        /// Create a set of sensor with more sensor.
        /// </summary>
        /// <param name="previousSet">Previous set of sensor.</param>
        /// <param name="newSensor">The new sensor</param>
        public SensorSet(Sensor[] previousSet, Sensor newSensor)
        {
            sensors = new List<Sensor>(previousSet.Length + 1);
            sensors.AddRange(previousSet);
            if (previousSet.Count(x => x.Equals(newSensor)) == 0) {
                sensors.Add(newSensor);
            }

            cost = Convert.ToInt32(sensors.Sum(x => x.Cost));
        }

        /// <summary>
        /// List of choosed sensor.
        /// </summary>
        private List<Sensor> sensors;
        public Sensor[] SensorList { get { return sensors.ToArray(); } }
        public Sensor this[int ii] { get { return sensors[ii]; }}
        public Sensor this[string ii] { get { return sensors.First(x => x.Name == ii); } }

        private int cost;
        /// <summary>
        /// Value of choosed sensors. Negative value means 'infinite'.
        /// </summary>
        public int TotalCost { get { return cost; } }
        public static int InfiniteCost { get { return int.MinValue; } }

        public static bool operator < (SensorSet s1, SensorSet s2) {
            if (s1.TotalCost < 0) { return false; }
            if (s2.TotalCost < 0) { return true; }
            return s1.TotalCost < s2.TotalCost;
        }

        public static bool operator > (SensorSet s1, SensorSet s2) {
            if (s1.TotalCost < 0) { return true; }
            if (s2.TotalCost < 0) { return false; }
            return s1.TotalCost > s2.TotalCost;
        }

        public static bool operator ==(SensorSet s1, SensorSet s2)
        {
            if (s1.TotalCost < 0 && s2.TotalCost < 0) { return true; }
            if (s1.TotalCost < 0 || s2.TotalCost < 0) { return false; }
            return s1.TotalCost == s2.TotalCost;
        }

        public static bool operator !=(SensorSet s1, SensorSet s2)
        {
            return !(s1 == s2);
        }

        public override string ToString()
        {
            string a = string.Format(@"This SensorSet has {0} Sensors in it. {1}",
                sensors.Count,
                sensors.Count == 0 ? "" : "Its total cost is " + this.TotalCost + ". ");
            if (sensors.Count > 0)
            {
                a += "\nAll sensors of this SensorSet are: ";
                foreach (Sensor s in sensors)
                {
                    a += string.Format("{0}, ", s.Name);
                }
            }
            a += Environment.NewLine;
            return a;
        }
    }

    /// <summary>
    /// Algorithm to solve the problem.
    /// </summary>
    public class Algorithm
    {
        public Algorithm(Target[] tList, Sensor[] sList)
        {
            // Initialized all Targets and Sensors.
            // All Dots will be sorted by x-coordinates.
            tlist = new List<Target>(tList.Length);
            tlist.AddRange(tList.OrderBy(t => t.X));

            slist = new List<Sensor>(sList.Length);
            slist.AddRange(sList.OrderBy(s => s.X));

            sensorSetList = new SensorSet[TargetList.Count, SensorList.Count];

            PostOutput += Algorithm_PostOutput;
        }

        void Algorithm_PostOutput(object sender, PostOutputEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("[OUTPUT] {0}", e.Lines.Select(x => x));
        }

        public event PostOutputEventHandler PostOutput;

        private List<Target> tlist;
        public List<Target> TargetList { get { return tlist; } }

        private List<Sensor> slist;
        public List<Sensor> SensorList { get { return slist; } }

        private SensorSet[,] sensorSetList;

        /// <summary>
        /// Solve the problem.
        /// </summary>
        /// <returns></returns>
        public bool Do()
        {
            // ### Process all SensorSets.
            PostOutput(this, new PostOutputEventArgs(new string[]{
                "---------------------------------------------------------",
                "-            Starting process all SensorSets.           -",
                "---------------------------------------------------------",
            }));

            for (int tt = 0; tt < TargetList.Count; tt++)
            {
                for (int ss = 0; ss < SensorList.Count; ss++)
                {
                    // ## OPT(T[tt], S[ss]) = Infinite          S[ss] never covers T[tt].
                    if (TargetList[tt] - SensorList[ss] > 1)
                    {
                        sensorSetList[tt, ss] = new SensorSet();
                        //PostOutput(this, new PostOutputEventArgs(new string[] {
                        //        "-----------------------------------------------------",
                        //        string.Format("A new SensorSet on target {0}, sensor {1}", tt+1, ss+1),
                        //        sensorSetList[tt, ss].ToString(),
                        //        "-----------------------------------------------------"
                        //        }));
                        continue;
                    }

                    // ## OPT(T[tt], S[ss]) = S[ss]             if tt=1 and S[ss] covers T[tt].
                    if (TargetList[tt] - SensorList[ss] <= 1 && tt == 0)
                    {
                        sensorSetList[tt, ss] = new SensorSet(SensorList[ss]);
                        PostOutput(this, new PostOutputEventArgs(new string[] {
                                string.Format("A new SensorSet on target {0} sensor {1}", tt+1, ss+1),
                                sensorSetList[tt, ss].ToString()
                                }));
                        continue;
                    }


                    // ## OPT(T[tt], S[ss]) = min(Opt(T[tt-1], S[k])       
                    //                                          + S[k].cost if S[k] not exists
                    foreach (var kk in Enumerable.Range(0, SensorList.Count)
                        .Select(x => sensorSetList[tt - 1, x])
                        .Where(x => x.TotalCost >= 0))
                    {
                        try { int t = sensorSetList[tt, ss].TotalCost; }
                        catch (NullReferenceException nre) { sensorSetList[tt, ss] = new SensorSet(); }
                        SensorSet newSet = new SensorSet(kk.SensorList, SensorList[ss]);
                        if (newSet < sensorSetList[tt, ss]) { sensorSetList[tt, ss] = newSet; }
                    }
                    PostOutput(this, new PostOutputEventArgs(new string[] {
                                string.Format("A new SensorSet on target {0} sensor {1}", tt+1, ss+1),
                                sensorSetList[tt, ss].ToString(),
                                }));
                }
            }

            // ### Find out the minimum SensorSet and its costs.
            SensorSet t_ss = sensorSetList[TargetList.Count - 1, 0];
            for (int ii = 1; ii < SensorList.Count; ii ++) {
                if (t_ss > sensorSetList[TargetList.Count - 1, ii]) {
                    t_ss = sensorSetList[TargetList.Count - 1, ii];
                }
            }
            PostOutput(this, new PostOutputEventArgs(new string[]{
                "",
                "=============RESULT==============",
                "The minimum set of sensors to solve the problem is the following:\n",
                t_ss.ToString()
            }));

            return true;
        }
    }

    #region PostOutput Event specifications
    public delegate void PostOutputEventHandler(object sender, PostOutputEventArgs e);
    public class PostOutputEventArgs : EventArgs
    {
        public PostOutputEventArgs(string[] lines) {
            Lines = lines;
        }
        public string[] Lines { get; set; }
    }
    #endregion
}
