﻿namespace input
{
    partial class formMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.btnNew = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.numMaxCost = new System.Windows.Forms.NumericUpDown();
            this.numMinCost = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinCost)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(656, 222);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape1,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(739, 258);
            this.shapeContainer1.TabIndex = 1;
            this.shapeContainer1.TabStop = false;
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.Location = new System.Drawing.Point(10, 10);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(720, 200);
            this.rectangleShape1.Click += new System.EventHandler(this.rectangleShape1_Click);
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 10;
            this.lineShape1.X2 = 730;
            this.lineShape1.Y1 = 110;
            this.lineShape1.Y2 = 110;
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(575, 222);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 23);
            this.btnNew.TabIndex = 4;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // numMaxCost
            // 
            this.numMaxCost.Location = new System.Drawing.Point(239, 222);
            this.numMaxCost.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numMaxCost.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMaxCost.Name = "numMaxCost";
            this.numMaxCost.Size = new System.Drawing.Size(77, 21);
            this.numMaxCost.TabIndex = 6;
            this.numMaxCost.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMaxCost.ValueChanged += new System.EventHandler(this.numMaxCost_ValueChanged);
            // 
            // numMinCost
            // 
            this.numMinCost.Location = new System.Drawing.Point(156, 222);
            this.numMinCost.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numMinCost.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMinCost.Name = "numMinCost";
            this.numMinCost.Size = new System.Drawing.Size(77, 21);
            this.numMinCost.TabIndex = 7;
            this.numMinCost.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMinCost.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 224);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 12);
            this.label1.TabIndex = 8;
            this.label1.Text = "Sensor Cost Range";
            // 
            // formMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 258);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numMinCost);
            this.Controls.Add(this.numMaxCost);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.shapeContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formMain";
            this.Text = "Input Generater";
            ((System.ComponentModel.ISupportInitialize)(this.numMaxCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinCost)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        public Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.NumericUpDown numMaxCost;
        private System.Windows.Forms.NumericUpDown numMinCost;
        private System.Windows.Forms.Label label1;
    }
}

