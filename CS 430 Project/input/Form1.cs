﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace input
{
    public partial class formMain : Form
    {
        public formMain()
        {
            InitializeComponent();
        }

        private void rectangleShape1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Rectangle Shape Clicked! ({0},{1})", (e as MouseEventArgs).X, (e as MouseEventArgs).Y);
            MouseEventArgs me = e as MouseEventArgs;
            if (me.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (me.X > 10 && me.X < 730 && me.Y > 10 && me.Y < 210)
                {
                    // Draw
                    if (me.Y < 110)
                    {
                        Dot d = new Target(new Point(me.X, me.Y));
                        dots.Add(d);
                        d.dot.Visible = true;
                    }
                    else if (me.Y > 110)
                    {
                        Dot d = new Sensor(new Point(me.X, me.Y));
                        dots.Add(d);
                        d.dot.Visible = true;
                    }
                }
            }
            else if (me.Button == System.Windows.Forms.MouseButtons.Right)
            {
                foreach (var x in dots.Where(x => !x.isDelete &&
                    distance(new Point(me.X, me.Y), x.location) < 4))
                {
                    x.delete();
                }
            }
        }

        private double distance(Point point1, Point point2)
        {
            double d = (point1.X - point2.X) * (point1.X - point2.X) + (point1.Y - point2.Y) * (point1.Y - point2.Y);
            return Math.Sqrt(d);
        }

        public List<Dot> dots = new List<Dot>();

        private void btnNew_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Unsaved changes will be lost!", "Really?", MessageBoxButtons.YesNo) == 
                System.Windows.Forms.DialogResult.Yes)
            {
                foreach (var d in dots.Where(x => !x.isDelete))
                {
                    d.delete();
                }
                dots = new List<Dot>();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool haveDeleted = false, once = true;
            foreach (Target d in dots.Where(x => x.type == DotType.Target))
            {
                bool next = false;
                foreach (Sensor s in dots.Where(x => x.type == DotType.Sensor))
                {
                    if (distance(d.location, s.location) < 100)
                    {
                        next = true;
                        break;
                    }
                }
                if (!next) { 
                    haveDeleted = true;
                    if (haveDeleted && once)
                    {
                        if (MessageBox.Show("Because some Target points have no sensor connected\nThey will be deleted.\nPress yes to continue and no to cancel.", "", MessageBoxButtons.YesNo) ==
                            System.Windows.Forms.DialogResult.No)
                        {
                            return;
                        }
                        once = false;
                    }
                    d.delete(); 
                }
            }

            
            saveFileDialog1.ShowDialog();
            if (System.IO.File.Exists(saveFileDialog1.FileName))
            {
                System.IO.File.Delete(saveFileDialog1.FileName);
            }

            try
            {
                System.IO.Stream st = saveFileDialog1.OpenFile();
                if (st != null)
                {
                    System.IO.StreamWriter w = new System.IO.StreamWriter(st);

                    Random r = new Random();
                    foreach (Dot t in dots.Where(x => !x.isDelete))
                    {
                        w.WriteLine("{0} {1} {2} {3}",
                            t.type == DotType.Target ? "T" : (t.type == DotType.Sensor ? "S" : "N"),
                            (float)(t.location.X - 10) / 100, 
                            (float)(t.location.Y - 110) / 100,
                            t.type == DotType.Target ? "0" : r.Next(Decimal.ToInt32(numMinCost.Value), 
                                Decimal.ToInt32(numMaxCost.Value)).ToString());
                    }

                    w.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured:\n" + ex.ToString());
            }

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (numMinCost.Value > numMaxCost.Value)
            {
                numMaxCost.Value = numMinCost.Value;
            }
        }

        private void numMaxCost_ValueChanged(object sender, EventArgs e)
        {
            if (numMaxCost.Value < numMinCost.Value)
            {
                numMinCost.Value = numMaxCost.Value;
            }
        }
    }

    public class DotDeleteEventArgs : EventArgs
    {
        public DotDeleteEventArgs(int i) { index = i; }
        public int index;
    }

    public enum DotType { Non, Target, Sensor, }

    public partial class Dot
    {
        public Dot(Color c, Point p)
        {
            dot.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            dot.FillColor = c;
            dot.Size = new Size(4, 4);
            dot.Location = p;
            // dot.Click += dot_Click;
            Program.form.shapeContainer1.Shapes.Add(dot);
            location = p;
        }

        public void delete()
        {
            this.dot.Visible = false;
            isDelete = true;
        }

        void dot_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Dot Clicked! ({0},{1})", (e as MouseEventArgs).X, (e as MouseEventArgs).Y);

            if ((e as MouseEventArgs).Button == MouseButtons.Right)
            {
                isDelete = true;
                dot.Visible = false;
            }
        }
        public Microsoft.VisualBasic.PowerPacks.OvalShape dot = new Microsoft.VisualBasic.PowerPacks.OvalShape();
        public DotType type = DotType.Non;
        public Point location = new Point(0, 0);
        public bool isDelete;
    }

    public partial class Target : Dot
    {
        public Target(Point p) : base(Color.Red, p)
        {
            type = DotType.Target;

        }
    }

    public partial class Sensor : Dot{
        public Sensor(Point p) : base (Color.Blue, p) {
            type = DotType.Sensor;
        }
    }
}
